<?php

namespace Drupal\example_ajax_autocomplete\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {



  public function handleAutocomplete(Request $request, $entity_type) {
    $results = [];

      // @todo: Apply logic for generating results based on typed_string and other
      // arguments passed.
      for ($i = 0; $i < 6; $i++) {
        $results[] = [
          'value' => $entity_type.'_' . $i,
          'label' => $entity_type.' - N°' . $i,
        ];
      }

    return new JsonResponse($results);

  }

}
